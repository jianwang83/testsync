﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace TestSync
{
    public class SyncHelper
    {
        private ConcurrentDictionary<string, object> locks = new ConcurrentDictionary<string, object>();
        private ConcurrentDictionary<string, object> responses = new ConcurrentDictionary<string, object>();
        public static SyncHelper Instance = new SyncHelper();

        private SyncHelper()
        {
        }

        /// <summary>
        /// 从字典中获取一个锁对象
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private object GetLock(string id)
        {
            if (!locks.ContainsKey(id))
            {
                locks[id] = new object();
            }
            return locks[id];
        }

        /// <summary>
        /// 从字典中剔除锁对象
        /// </summary>
        /// <param name="id"></param>
        private void RemoveLock(string id)
        {
            object result = null;
            if (locks.ContainsKey(id))
            {
                locks.TryRemove(id, out result);
            }
        }

        /// <summary>
        /// 唤醒等待线程
        /// </summary>
        /// <param name="id"></param>
        /// <param name="response"></param>
        public void Pulse(string id, object response)
        {
            object lockObj = GetLock(id);
            lock (lockObj)
            {
                responses[id] = response;
                Monitor.PulseAll(lockObj);
            }
            RemoveLock(id);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="scanTimeout">秒数</param>
        /// <param name="result">返回等待的结果</param>
        /// <returns></returns>
        public bool Wait(string id, int scanTimeout, out object result)
        {
            bool flag = false;
            result = null;
            if (!this.ContainsKey(id))
            {
                object synRoot = this.GetLock(id);
                lock (synRoot)
                {
                    flag = Monitor.Wait(synRoot, scanTimeout * 1000);
                }
            }
            if (flag)
            {
                this.responses.TryGetValue(id, out result);
            }
            this.RemoveKey(id);
            this.RemoveLock(id);
            return flag;
        }

        /// <summary>
        /// 返回结果是否包含id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private bool ContainsKey(string id)
        {
            return this.responses.ContainsKey(id);
        }

        /// <summary>
        /// 从返回结果中剔除id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private bool RemoveKey(string id)
        {
            object result = null;
            return this.responses.TryRemove(id, out result);
        }
    }
}
