﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TestSync
{
    class Program
    {
        public static object lockObj = new object();

        static void Main(string[] args)
        {
            Thread t = new Thread(Thread1);
            t.Start();
            Thread t2 = new Thread(Thread2);
            t2.Start();
            Console.ReadLine();

            SyncHelper temp = SyncHelper.Instance;
        }


        static void Thread1()
        {
            Object result = null;
            SyncHelper.Instance.Wait("123", 5, out result);
            Console.WriteLine(result);
            Console.WriteLine("Thread1结束");
        }

        static void Thread2()
        {
            Thread.Sleep(3000);
            SyncHelper.Instance.Pulse("123", "456");
            Console.WriteLine("Thread2结束");
        }
    }
}
